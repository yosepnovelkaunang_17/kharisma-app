<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Kharisma App</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-free/css/all.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/icheck-bootstrap/icheck-bootstrap.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/adminlte.min.css') ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="<?= base_url('Welcome') ?>"><b>Kharisma</b> App</a>
    </div>

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Daftar baru keanggotaan</p>
        <?php echo $this->session->flashdata('err_message'); ?>
        <?php echo form_open_multipart('register_user/reg'); ?>
        <div class="input-group mb-3">
          <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Lengkap" pattern="[A-Za-z ]{1,}" title="Nama yang valid tidak menggunakan angka ataupun simbol" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" id="username" name="username" class="form-control" placeholder="Username" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="date" id="tanggal" name="tanggal" class="form-control" placeholder="" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-birthday-cake"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="password" onkeyup='check();' name="password" class="form-control" pattern=".{6,}" title="Minimal karakter adalah 6" placeholder="Kata Sandi" required>
          <div class="input-group-append">
            <div class="input-group-text">
            <span class="fas fa-eye" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="repass" onkeyup='check();' name="repass" class="form-control" pattern=".{6,}" title="Minimal karakter adalah 6" placeholder="Ketik ulang sandi" required>
          <div class="input-group-append">
            <div class="input-group-text">
            <span id='message'></span>
            <span class="fas fa-eye" onmouseover="mouseoverPass2();" onmouseout="mouseoutPass2();">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree" oninvalid="this.setCustomValidity('Pastikan anda telah menyetujui dan membaca ketentuan dari kami')" required>
              <label for="agreeTerms">
                Saya setuju pada <a href="" data-toggle="modal" data-target="#termModal" readonly>Ketentuan</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <input type="submit" class="btn btn-primary btn-block"></input>
          </div>
          <!-- /.col -->
        </div>
        </form>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
  <!-- /.register-box -->
  <a href="<?= base_url('login') ?>" class="text-center">Sudah punya akun?</a>
  <?php $this->load->view("_partials/scripts.php") ?>
  <?php $this->load->view("_partials/modals.php") ?>
</body>

</html>