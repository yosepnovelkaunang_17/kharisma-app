          <!--SCREENSHOT-->
          <div id="carousel" class="carousel slide carousel-fade mb-3" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <a target="_blank" href="https://classroom.kharisma.ac.id/">
                     <picture>
                      <source srcset="assets/banner/ss1.png" media="(min-width: 1400px)">
                      <source srcset="assets/banner/ss1.png" media="(min-width: 769px)">
                       <source srcset="assets/banner/ss1.png" media="(min-width: 577px)">
                      <img srcset="assets/banner/ss1.png" alt="responsive image" class="d-block img-fluid">
                    </picture>
                </a>
            </div>
            <!-- /.carousel-item -->
            <div class="carousel-item">
                <a target="_blank" href="https://siska.kharisma.ac.id/">
                     <picture>
                      <source srcset="assets/banner/ss2.png" media="(min-width: 1400px)">
                      <source srcset="assets/banner/ss2.png" media="(min-width: 769px)">
                       <source srcset="assets/banner/ss2.png" media="(min-width: 577px)">
                      <img srcset="assets/banner/ss2.png" alt="responsive image" class="d-block img-fluid">
                    </picture>
                </a>
            </div>
            <!-- /.carousel-item -->
            <div class="carousel-item">
                <a target="_blank" href="https://www.kharisma.ac.id/">
                     <picture>
                      <source srcset="assets/banner/ss3.png" media="(min-width: 1400px)">
                      <source srcset="assets/banner/ss3.png" media="(min-width: 769px)">
                       <source srcset="assets/banner/ss3.png" media="(min-width: 577px)">
                      <img srcset="assets/banner/ss3.png" alt="responsive image" class="d-block img-fluid">
                    </picture>
                </a>
            </div>
            <!-- /.carousel-item -->
        </div>
        <!-- /.carousel-inner -->
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->

            <!--END OF SCREENSHOT-->