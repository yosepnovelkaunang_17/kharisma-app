          <!-- LOGOUT MODAL-->
          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabesl" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Ingin logout?</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">Pilih tombol <i>Logout</i> untuk keluar.</div>
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                  <a class="btn btn-primary" href="<?= base_url('login/logout') ?>">Logout</a>
                </div>
              </div>
            </div>
          </div>
          <!-- END LOGOUT MODAL -->

          <!-- DELETE MODAL-->
          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabesl" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ingin menghapus data ini?</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">Aplikasi yang anda hapus tidak dapat dikembalikan</div>
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-danger" id="btn-delete" href="#">Hapus</a>
                </div>
              </div>
            </div>
          </div>
          <!-- END OF DELETE MODAL -->

          <!-- RATING MODAL -->
          <div class="modal fade" id="Rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabesl" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabesl">Nilai aplikasi ini</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>

                <form action="<?= base_url('desc_page/rate') ?>" method="post">
                  <div class="modal-body">
                    <div class="text">Berikan nilai dari 1 sampai 5</div>
                    <label class="radio-inline ">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" required> 1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2" required> 2
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3" required> 3
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio4" value="4" required> 4
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions" id="inlineRadio5" value="5" required> 5
                    </label>
                    <hr class="divider">
                    <div class="text">Comment</div>
                    <textarea name="txtcomment" id="txtcomment" cols="50" rows="5" placeholder="Enter Your Comment here!" maxlength="200"></textarea>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Beri nilai</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- END RATING MODAL -->

          <!-- TERMS MODAL-->
          <div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabesl" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Syarat dan Ketentuan :</h5>
                </div>
                <div class="modal-body">Seluruh data yang anda masukkan harus merupakan data yang valid</div>
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </div>
          <!-- END TERMS MODAL -->

          <!-- TERMS MODAL CREATOR -->
          <div class="modal fade" id="termModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabesl" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Syarat dan Ketentuan :</h5>
                </div>
                <div class="modal-body">Seluruh data yang anda masukkan harus merupakan data yang valid.
                Dan sebagai creator juga berhak untuk tunduk pada aturan, dimana aplikasi yang diupload sesuai kriteria.
                </div>
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </div>
          <!-- END TERMS MODAL CREATOR -->