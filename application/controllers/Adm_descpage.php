<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_descpage extends CI_Controller {
	public function index(){			
	    if ($this->session->userdata('Admin')){
		$id = $this->input->get('id');
		$this->load->model('apps_model');
		$this->load->model('Aplikasi_model');
		$data["get_ss"] = $this->Aplikasi_model->get_ss($id);
		$data["get_Creatorapp"] = $this->apps_model->get_Creatorapp($id);
		$data["get_review"] = $this->apps_model->getrate($id);
		$data["getappcat"] = $this->Aplikasi_model->get_app_cat($id);
		$data["maxversi"] = $this->Aplikasi_model->max_versi($id);
		$data["maxsize"] = $this->Aplikasi_model->max_size($id);
		$data["avgrate"] = $this->Aplikasi_model->avg_rate($id);
		$this->load->view('v_admin_appdeskripsi',$data);
	} else {
			    redirect('Welcome');
			}
	}


	public function actived($id)
	{
		$this->load->model('Aplikasi_model');	
		$idadm=$this->session->userdata('username');
		$nama=$this->Aplikasi_model->get_nama($id);
		$stripped= str_replace(' ', '_', $nama);
		$active=1;
		$this->Aplikasi_model->aktifasi($active, $id);
		redirect('adm_page');	
	}

	public function deactive($id) {
		$this->load->model('Aplikasi_model');	
		$idadm=$this->session->userdata('username');
		$nama=$this->Aplikasi_model->get_nama($id);
		$stripped= str_replace(' ', '_', $nama);
		$active=0;
		$this->Aplikasi_model->aktifasi($active, $id);
		redirect('adm_page');	
		}
}