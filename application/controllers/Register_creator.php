<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Register_creator extends CI_Controller

{

    public function __construct()

    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        // $this->load->library('upload');
    }

    public function index()

    {
         if ($this->session->userdata('logged_in')){
        redirect('Welcome');
        } else {
        $this->load->view('v_register_creator');
        }
    }
    public function reg_crea()

    {
        $this->load->model('register_model');
        $id = $this->input->post('nim');
        $nama = $this->input->post('nama');
        $password = $this->input->post('password');
        $repass = $this->input->post('repass');
        $foto = "assets/foto/creator/default.jpg";
        $tanggal = $this->input->post('tanggal');
        $nama_folder =  "assets/foto/creator/";
        $users_exist = $this->register_model->user_exist_creator($id);

        if((file_exists($nama_folder))&&(is_dir($nama_folder))){
        } else{
        mkdir($nama_folder, 0777, true);            
        }


        if ($password != $repass) {
            $this->session->set_flashdata('err_message', 'Password doesnt match');
            redirect('register_creator');
        } elseif (count($users_exist) >= 1) {
            $this->session->set_flashdata('err_message', 'username already exist');
            redirect('register_creator');
        } else {

            $config['upload_path']          = './assets/foto/creator/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $id;
            $config['overwrite']            = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $this->upload->do_upload('userfile');
            $this->register_model->set_creator($id, $nama, $password, $foto, $tanggal);
            redirect('login');
        }
    }
}