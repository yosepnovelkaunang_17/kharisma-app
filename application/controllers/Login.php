<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Login extends CI_Controller

{
    public function index()
    {
        if ($this->session->userdata('logged_in')){
        redirect('Welcome');
        } else {
        $this->load->view('v_login');
        }
    }

    public function c_login(){

        $this->load->model('apps_model');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $users = $this->apps_model->authdbuser($username, $password);
        $creator = $this->apps_model->authdbcreator($username, $password);
        $admin = $this->apps_model->authdbadm($username,$password);

        if (count($users) > 0) {
            $logindata = array(
                'iduser' => $users[0]->id,
                'username'  => $users[0]->username,
                'nama'  => $users[0]->nama,
                'password' => $users[0]->password,
                'tanggal' => $users[0]->tgl_lahir,
                'foto' => $users[0]->foto,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($logindata);
            redirect('Welcome');

        } else {
            if (count($creator) > 0) {
                $logindata = array(
                    'username'  => $creator[0]->id,
                    'password' => $creator[0]->password,
                    'nama' => $creator[0]->nama,
                    'foto' => $creator[0]->foto,
                    'tanggal' => $creator[0]->tgl_lahir,
                    'logged_in' => TRUE,                    
                    'Creator' => TRUE
                );

                $this->session->set_userdata($logindata);
                redirect('creator_page');

            } else {
                if (count($admin) > 0) {
                    $logindata = array(
                        'username' => $admin[0]->username,
                        'id'  => $admin[0]->id,
                        'nama' => $admin[0]->nama,
                        'password' => $admin[0]->password,
                        'logged_in' => TRUE,                    
                        'Admin' => TRUE
                    );
                $this->session->set_userdata($logindata);
                redirect('adm_page');
    
                } else {
                $this->session->set_flashdata('err_message', 'ID/Username/NIM atau Password salah');
                redirect('login');
            }
        }
    }
    }
    public function logout()
    {
        session_unset();
        session_destroy();
        redirect('welcome');
    }
}