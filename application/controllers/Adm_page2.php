<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_page2 extends CI_Controller { 
	public function index()
	{
		$this->load->model('apps_model');
			if ($this->session->userdata('Admin')){
			$idadm=$this->session->userdata('username');
			$data["get_app"] = $this->apps_model->get_app_n_creator();
			$data["get_cat"] = $this->apps_model->get_cat();
			$this->load->view('v_adminpage2', $data);
			} else {
			    redirect('Welcome');
			}
	}
}	