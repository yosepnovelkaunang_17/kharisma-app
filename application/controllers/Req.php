<?php defined('BASEPATH') or exit('No direct script access allowed');

class Req extends CI_Controller {
    public function __construct()

    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        // $this->load->library('upload');
    }

    public function index()

    {
    if ($this->session->userdata('Creator')){
        redirect('Welcome');
    }  else {
        $this->load->model('apps_model');        
        $this->load->view('v_updateuser');
			}
    }

    public function update()

    {
        $path = $_FILES['userfile']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $this->load->model("Aplikasi_model");  
        $id = $this->session->userdata('iduser');                
        $password = $this->input->post('password');
        $user = $this->input->post('user');
        $tanggal =  $this->input->post('tanggal'); 
        $nama = $this->input->post('nama');
        $stripped = str_replace(' ', '', $id);
        $foto = "assets/foto/user/$id/$stripped.$ext";
        $nama_folder =  "assets/foto/user/$id/";

        if((file_exists($nama_folder))&&(is_dir($nama_folder))){
        } else{
        mkdir($nama_folder, 0777, true);            
        }

            $config['upload_path']          = "./assets/foto/user/$id";
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            $config['file_name']            = $stripped;
            $config['overwrite']            = TRUE;

            $this->load->library('upload', $config);            
            $this->upload->do_upload('userfile');
            $this->Aplikasi_model->update_user($id, $nama, $user, $password, $foto, $tanggal);
            
            redirect('welcome');
    }
}