<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_page extends CI_Controller { 
	public function index()
	{
		$this->output->enable_profiler(TRUE);
		$this->load->model('apps_model');
			if ($this->session->userdata('Admin')){
			$idadm=$this->session->userdata('nama');
			$data["get_app"] = $this->apps_model->get_app_n_creator();
			$data["get_cat"] = $this->apps_model->get_cat();
			$this->load->view('v_adminpage', $data);
			} else {
			    redirect('Welcome');
			}
	}
}	