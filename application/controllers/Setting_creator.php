<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Setting_creator extends CI_Controller

{
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index(){
    if ($this->session->userdata('Creator')){
        $this->load->model('apps_model');        
        $this->load->view('v_Setting_Creator');
    }  else {
			    redirect('Welcome');
			}
    }

    public function update(){

        $path = $_FILES['userfile']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $this->load->model("Aplikasi_model");  
        $id = $this->session->userdata('username');
        $tanggal =  $this->input->post('tanggal'); 
        $password = $this->input->post('password');
        $nama = $this->input->post('nama');
        $stripped = str_replace(' ', '', $id);
        $foto = "assets/foto/creator/$id/$stripped.$ext";
        $nama_folder =  "assets/foto/creator/$id/";
        unlink($foto);

        if((file_exists($nama_folder))&&(is_dir($nama_folder))){
        } else{
        mkdir($nama_folder, 0777, true);            
        }

            $config['upload_path']          = "./assets/foto/creator/$id/";
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $stripped;
            $config['overwrite']            = TRUE;

            $this->load->library('upload', $config);            
            $this->upload->do_upload('userfile');
            $this->Aplikasi_model->update_creator($id, $nama, $password, $foto, $tanggal);
            session_unset();
            session_destroy();
            redirect('welcome');
    }
}