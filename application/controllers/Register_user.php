<?php defined('BASEPATH') or exit('No direct script access allowed');

class Register_user extends CI_Controller

{
    public function __construct()

    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index()

    {
         if ($this->session->userdata('logged_in')){
        redirect('Welcome');
        } else {
        $this->load->view('v_register_user');
        }
    }

    public function reg()

    {
        $this->load->model('register_model');
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $repass = $this->input->post('repass');
        $foto = "assets/foto/user/default.jpg";
        $tanggal = $this->input->post('tanggal');
        $nama_folder =  "assets/foto/user/";

        if((file_exists($nama_folder))&&(is_dir($nama_folder))){
        } else{
        mkdir($nama_folder, 0777, true);            
        }

        $users_exist = $this->register_model->user_exist_user($username);

        if ($password != $repass) {
            $this->session->set_flashdata('err_message', 'Password doesnt match');
            redirect('register_user');
        } elseif (count($users_exist) >= 1) {
            $this->session->set_flashdata('err_message', 'username already exist');
            redirect('register_user');
        } else {
            $config['upload_path']          = './assets/foto/user';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $username;
            $config['quality']              = '50%';
            $config['overwrite']            = TRUE;
            $config['max_width']            = 500;
            $config['max_height']           = 500;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('userfile');
            $this->register_model->set_user($nama, $username, $password, $foto, $tanggal, $email);
            redirect('login');
        }
    }
}