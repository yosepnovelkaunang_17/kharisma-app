-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: appstore
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Table untuk akun admin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Admin','admin','admin1');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps`
--

DROP TABLE IF EXISTS `apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_creator` varchar(50) DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `icon` varchar(225) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `active` int(1) NOT NULL,
  `date_created` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_creator_apps_idx` (`id_creator`),
  CONSTRAINT `id_creator_apps` FOREIGN KEY (`id_creator`) REFERENCES `creator` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Table informasi sebuah aplikasi';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps`
--

LOCK TABLES `apps` WRITE;
/*!40000 ALTER TABLE `apps` DISABLE KEYS */;
INSERT INTO `apps` VALUES (1,'52016002','Youtube','assets/logos/52016002/Youtube.jpg','Aplikasi untuk stream dan nonton video dan upload video',1,'2021-02-13'),(2,'52016035','Github','assets/logos/52016035/Github.png','Aplikasi Repository kode program dari aplikasi yang dibuat',1,'2021-02-18'),(3,'52016035','Go-Food','assets/logos/52016035/Go-Food.jpg','Aplikasi go-food',1,'2021-02-18'),(4,'52016002','Visual Studio Code','assets/logos/52016002/Visual_Studio_Code.png','Visual Studio Code editor kode untuk Windows, Linux and macOS',1,'2021-02-18');
/*!40000 ALTER TABLE `apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Table untuk kategori sebuah aplikasi';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Produk'),(2,'Tugas Akhir'),(3,'Tugas Aplikasi'),(4,'Tugas Dosen');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_apps`
--

DROP TABLE IF EXISTS `category_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_apps` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`id_apps`),
  KEY `id_category_idx` (`id_category`),
  CONSTRAINT `id_apps` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_category` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Table untuk penampung suatu kategori aplikasi';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_apps`
--

LOCK TABLES `category_apps` WRITE;
/*!40000 ALTER TABLE `category_apps` DISABLE KEYS */;
INSERT INTO `category_apps` VALUES (3,1,1),(4,1,3),(5,2,1),(6,3,2),(7,3,4),(8,4,1),(9,4,2);
/*!40000 ALTER TABLE `category_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creator`
--

DROP TABLE IF EXISTS `creator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creator` (
  `id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table untuk konten kreator';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creator`
--

LOCK TABLES `creator` WRITE;
/*!40000 ALTER TABLE `creator` DISABLE KEYS */;
INSERT INTO `creator` VALUES ('52016001','Ahmad','haha12','assets/foto/creator/52016001/52016001.jpg','1998-06-12'),('52016002','Arnold','homba12','assets/foto/creator/52016002/52016002.jpg','1999-12-12'),('52016003','Abed','joba12','assets/foto/creator/52016003/52016003.png','1991-01-01'),('52016035','Yosep Novel Kaunang','kharisma','assets/foto/creator/52016035/52016035.jpg','1998-02-11');
/*!40000 ALTER TABLE `creator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorit`
--

DROP TABLE IF EXISTS `favorit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_apps_favorit_idx` (`id_apps`),
  KEY `id_user_favorit_idx` (`id_user`),
  CONSTRAINT `id_apps_favorit` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_user_favorit` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Table untuk menampung jumlah favorit berdasarkan user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorit`
--

LOCK TABLES `favorit` WRITE;
/*!40000 ALTER TABLE `favorit` DISABLE KEYS */;
INSERT INTO `favorit` VALUES (3,1,2,'2021-02-18'),(4,4,2,'2021-02-18');
/*!40000 ALTER TABLE `favorit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `rating` decimal(10,0) DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_apps_idx` (`id_apps`),
  KEY `id_user_idx` (`id_user`),
  KEY `id_log_idx` (`id_log`),
  CONSTRAINT `idapp` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idlog` FOREIGN KEY (`id_log`) REFERENCES `update_log` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Table untuk menampung review user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,1,2,2,'2021-02-13',3,'Nilai contoh '),(2,4,2,5,'2021-02-18',5,'Aplikasi ini sangat bagus\r\n');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `screenshot_apps`
--

DROP TABLE IF EXISTS `screenshot_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `screenshot_apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `screenshot` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_apps_ss_idx` (`id_apps`),
  CONSTRAINT `id_apps_ss` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Table untuk menampung gambar atau hasil tangkapan layar dari aplikasi tertentu';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `screenshot_apps`
--

LOCK TABLES `screenshot_apps` WRITE;
/*!40000 ALTER TABLE `screenshot_apps` DISABLE KEYS */;
INSERT INTO `screenshot_apps` VALUES (1,1,'assets/apps/52016002/1/screenshots/Screenshot1.jpg','2021-02-13 12:43:23',0),(2,1,'assets/apps/52016002/1/screenshots/Screenshot2.jpg','2021-02-13 12:43:31',0),(3,1,'assets/apps/52016002/1/screenshots/Screenshot3.png','2021-02-13 12:43:42',0);
/*!40000 ALTER TABLE `screenshot_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_log`
--

DROP TABLE IF EXISTS `search_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `keyword` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_idx` (`id_user`),
  CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Table untuk menampung user sering mencari aplikasi apa';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_log`
--

LOCK TABLES `search_log` WRITE;
/*!40000 ALTER TABLE `search_log` DISABLE KEYS */;
INSERT INTO `search_log` VALUES (1,3,'2021-02-13','a'),(2,3,'2021-02-13','a'),(3,3,'2021-02-13','z'),(4,3,'2021-02-13','a'),(5,2,'2021-02-18','Produk'),(6,2,'2021-02-18','Produk'),(7,2,'2021-02-18','Tugas Aplikasi'),(8,2,'2021-02-18','Tugas Aplikasi'),(9,2,'2021-02-18','Tugas Akhir');
/*!40000 ALTER TABLE `search_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unduhan`
--

DROP TABLE IF EXISTS `unduhan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unduhan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idapps_idx` (`id_apps`),
  KEY `iduser_idx` (`id_user`),
  CONSTRAINT `id_apps_unduhan` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_user_unduhan` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Table untuk menampung berapa banyak user yang melakukan unduhan dengan menyimpan waktu';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unduhan`
--

LOCK TABLES `unduhan` WRITE;
/*!40000 ALTER TABLE `unduhan` DISABLE KEYS */;
INSERT INTO `unduhan` VALUES (1,1,2,'2021-02-13'),(2,1,2,'2021-02-13'),(3,1,2,'2021-02-13'),(4,1,2,'2021-02-13'),(5,1,2,'2021-02-13'),(6,1,2,'2021-02-13'),(7,1,2,'2021-02-13'),(8,1,2,'2021-02-13'),(9,1,2,'2021-02-18'),(10,NULL,NULL,'2021-02-18');
/*!40000 ALTER TABLE `unduhan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `update_log`
--

DROP TABLE IF EXISTS `update_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `update_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `tanggal_log` datetime DEFAULT NULL,
  `versi` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `deskripsi` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `apk` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `size` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idapps_idx` (`id_apps`),
  CONSTRAINT `idapps` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table untuk log pembaharuan';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `update_log`
--

LOCK TABLES `update_log` WRITE;
/*!40000 ALTER TABLE `update_log` DISABLE KEYS */;
INSERT INTO `update_log` VALUES (1,1,'2021-02-13 12:36:08','1.0','Aplikasi pertama kali dimasukkan','assets/apps/52016002/1/10.rar',30.00),(2,1,'2021-02-13 12:37:42','1.1','Update fitur baru','assets/apps/52016002/1/11.rar',32.00),(3,2,'2021-02-18 01:58:46','1.0','Aplikasi pertama kali dimasukkan','assets/apps/52016035/2/10.zip',10.00),(4,3,'2021-02-18 01:59:54','1.3','Aplikasi pertama kali dimasukkan','assets/apps/52016035/3/13.zip',29.00),(5,4,'2021-02-18 02:12:19','1.2','Aplikasi pertama kali dimasukkan','assets/apps/52016002/4/12.zip',10.00);
/*!40000 ALTER TABLE `update_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `foto` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Table untuk user umum';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Yosep Novel Kaunang','Novel','kharisma','assets/foto/user/1/1.png','1999-02-11'),(2,'Novel K','Novel2','Novel2','assets/foto/user/2/2.png','1992-01-12'),(3,'Marley','M12','koko12','assets/foto/user/3/3.png','1992-02-01');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_log`
--

DROP TABLE IF EXISTS `view_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apps` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_apps_viewlog_idx` (`id_apps`),
  KEY `id_user_viewlog_idx` (`id_user`),
  CONSTRAINT `id_apps_viewlog` FOREIGN KEY (`id_apps`) REFERENCES `apps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_user_viewlog` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Table untuk menampung jumlah lihat dari suatu user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_log`
--

LOCK TABLES `view_log` WRITE;
/*!40000 ALTER TABLE `view_log` DISABLE KEYS */;
INSERT INTO `view_log` VALUES (1,1,2,'2021-02-13'),(2,1,2,'2021-02-13'),(3,1,2,'2021-02-13'),(4,1,2,'2021-02-13'),(5,1,2,'2021-02-13'),(6,1,3,'2021-02-13'),(7,1,2,'2021-02-16'),(8,1,2,'2021-02-18'),(9,1,2,'2021-02-18'),(10,3,2,'2021-02-18'),(11,2,2,'2021-02-18'),(12,1,2,'2021-02-18'),(13,1,2,'2021-02-18'),(14,3,2,'2021-02-18'),(15,3,2,'2021-02-18'),(16,1,2,'2021-02-18'),(17,1,2,'2021-02-18'),(18,1,2,'2021-02-18'),(19,4,2,'2021-02-18'),(20,4,2,'2021-02-18'),(21,4,2,'2021-02-18'),(22,4,2,'2021-02-18');
/*!40000 ALTER TABLE `view_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'appstore'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-27 15:21:56
